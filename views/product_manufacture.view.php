<!--thêm link css-->
    <link rel="stylesheet" type="text/css" href="public/css/style-product_manufacture.css">
<!--bây giờ code thôi-->
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-10 col-sm-3">
                <div class="Category">
                    <h5 class="title_category_name">Hãng sản xuất</h5>
                    <hr class="hr-pro_manufacture">

                     <?php foreach ($data['name_manu'] as $product) : ?>
              <dd><a href="sanphamtheohang-<?= $product->MaHangSanXuat?>"><?= $product->TenHangSanXuat ?></a></dd>
               <?php endforeach ?>
                 
                    

                    <h5 class="title_category_price">Chọn mức giá</h5>
                    <hr class="hr-pro_manufacture">
                    <dd><a href="product_manufacture.php">Dưới 3 triệu</a></dd>
                    <dd><a href="product_manufacture.php">Từ 3-7 triệu</a></dd>
                    <dd><a href="product_manufacture.php">Từ 7-20 triệu</a></dd>
                    <dd><a href="product_manufacture.php">Trên 20 Triệu</a></dd>
                </div>
            </div>
            <div class="col-md-9 col-xs-10 col-sm-9">
                <div class="pro_manufacture">
                    <h5>Các sản phẩm theo hãng sản xuất</h5>

                    <!-- 1 HÀNG THÌ CHỈ HIỂN THỊ 3 SẢN PHẨM-->
                    <div class="row">
                       <?php foreach ($data['product'] as $product) : ?>  
                            <div class="pro-item">
                                <div class="pro-item-header">
                                    <a href="sanpham/<?= $product->MaSanPham ?>"><img
                                            src="public/product/<?= $product->HinhURL?>"
                                            alt=""></a>
                                </div>
                                <div class="pro-item-body">
                                    <p class="pro-item-title"><?=$product->TenSanPham?></p>
                                    <p class="pro-item-price">
                                        <span><?=number_format($product->GiaSanPham)?>VNĐ</span>
                                    </p>
                                </div>
                                <div class="pro-item-caption">
                                    <a class="beta-btn primary" href="sanpham/<?= $product->MaSanPham ?>">Chi tiết sản phẩm <i
                                            class="fa fa-chevron-right"></i></a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                             <?php endforeach ?>
                        </div>
                    </div>
                </div>

        </div>

    </div>
</div>