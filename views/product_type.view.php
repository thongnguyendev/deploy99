   <!--thêm file css vào -->
   <link rel="stylesheet" type="text/css" href="public/css/style-product_type.css">

   <!--phần code-->
       <!-- content -->
    <div id="content">
     <div class="container">
         <div class="row">
       <div class="col-md-3 col-xs-10 col-sm-3">
           <div class="Category">
               <h5 class="title_category_name">Loại sản phẩm</h5>
               <hr class="hr-pro_type">
             <?php foreach ($data['nameType'] as $product) : ?>
              <dd><a href="sanphamtheoloai-<?= $product->MaLoaiSanPham ?>"><?= $product->TenLoaiSanPham ?></a></dd>
               <?php endforeach ?>
              
            <h5 class="title_category_price">Chọn mức giá</h5>
            <hr class="hr-pro_type">
             <dd><a href="#" >Dưới 3 triệu</a></dd>
             <dd><a href="#" >Từ 3-7 triệu</a></dd>
             <dd><a href="#" >Từ 7-20 triệu</a></dd>
             <dd><a href="#" >Trên 20 Triệu</a></dd>
            
              

           </div>
       </div>
       <div class="col-md-9 col-xs-10 col-sm-9">
           <div class="pro_type">
               <h5>Các sản phẩm theo loại</h5>
                
               <!-- 1 HÀNG THÌ CHỈ HIỂN THỊ 3 SẢN PHẨM-->
                <div class="row">   
                           <?php foreach ($data['product'] as $product) : ?>         
                        <div class="pro-item">
                            <div class="pro-item-header">
                                <a href="sanpham/<?= $product->MaSanPham ?>"><img
                                        src="public/product/<?= $product->HinhURL?>"
                                        alt=""></a>
                            </div>
                            <div class="pro-item-body">
                                <p class="pro-item-title"><?= $product->TenSanPham ?></p>
                                <p class="pro-item-price">
                                    <strong><?= number_format($product->GiaSanPham )?> VNĐ</strong>
                                </p>
                            </div>
                            <div class="pro-item-caption">
                                <a class="beta-btn primary" href="sanpham/<?= $product->MaSanPham ?>">Chi tiết sản phẩm <i
                                        class="fa fa-chevron-right"></i></a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <?php endforeach ?>
           </div>
       </div>
      </div>
     
     </div>

    </div>
    </div>