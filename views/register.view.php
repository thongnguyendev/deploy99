   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
    <!--CSS CHO đăng ký và js tự code-->

    <link rel="stylesheet" type="text/css" href="public/css/style-register.css">
    
<!-- check-->
<?php
//<script src="public/js/style-register.js" type="text/javascript"></script>
require_once'config.php';
if (isset($_POST['dangky'])) {
    $errMsg = '';
    // print_r($_POST['dangky']);
    // Get data from FROM
    $fullname = $_POST['name'];
    $username = $_POST['user_name'];
    $password = $_POST['user_password'];
    $cPwd = $_POST['confirm_password'];
    $capcha = $_POST['reCaptcha'];
    $day=$_POST['day'];
    $month=$_POST['month'];
    $year=$_POST['year'];
    $country=$_POST['country'];


    if ($username == '')
        $errMsg = 'Tên đăng nhập không được để trống!';
    if ($password == '')
        $errMsg = 'Mật khẩu không được để trống!';
    if ($cPwd == '')
        $errMsg = 'Mật khẩu xác thực không được để trống!';
     if ($capcha == '')
        $errMsg = 'Mã kiểm tra không được để trống!';    

    if ($errMsg == '') {
        try {
            
            // lấy dữ liệu ở database lên check xem có user nào trùng k
           $result = "SELECT * FROM taikhoan  WHERE TenDangNhap='$username'";
           if(count_chars($result) >= 1){
              echo "User exist";
             echo "<script>window.location.replace('register.php');</script>";
            //exit;
            }
           //print_r($stmt);
             else if(LEN($password) <= 5 || len($password) >= 30)
             {
		  $errMsg = 'Mật khẩu phải có ít nhất 6 kí tự và không quá 30 kí tự!'; 
           echo "<script>window.location.replace('register.php');</script>";
              print_r($password);	
             }
             
             else if($password != $cPwd)
             {
		  $errMsg = 'Mật khẩu xác thực không trùng khớp!'; 
        echo "<script>window.location.replace('register.php');</script>";
            }
           else{
                $stmt = $connect->prepare('INSERT INTO taikhoan (TenDangNhap, MatKhau, TenHienThi, DiaChi,MaLoaiTaiKhoan)
             VALUES ( :username, :password,:fullname, :addr,:loai)');
            $stmt->execute(array(
                ':username' => $username,
                ':password' => $password,
                ':fullname' => $fullname,
                ':addr' => $country,
                ':loai'=>1
                
            ));
             echo "<script>window.location.replace(' register.php?action=joined');</script>";
            exit;
           }
            
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'joined') {
    $errMsg = 'Đăng ký tài khoản thành công.Bây giờ bạn có thể <a href="login.php">Đăng nhập</a>';
}
?>


<!--code html-->
     <div id="content">
        <div class="container">
            <?php
                if (isset($errMsg)) {
                    echo '<div style="color:#FF0000;text-align:center;font-size:17px;">' . $errMsg . '</div>';
                }
                ?>
            <form class="well form-horizontal" action=" " method="post" id="contact_form">
                <fieldset>
                
                    <!-- Form Name -->
                    <legend>
                        <center>
                            <h2><b>Thông tin đăng ký tài khoản</b></h2>
                        </center>
                    </legend><br>
        
                    <!-- Text input-->
                     <div class="col-md-12 infomation-title"><h5>Thông tin cá nhân</h5></div>
                    <!-- Text input-->
        
                    <div class="form-group">
                        <label class="col-md-4 control-label">Họ tên của bạn</label>
                        <div class="col-md-4 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input name="name" value="<?php if (isset($_POST['fullname'])) echo $_POST['fullname'] ?>" placeholder="Nhập họ tên" class="form-control" type="text">
                            </div>
                        </div>
                    </div>
                    <!-- Text input option DD/MM/YYYY-->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Ngày sinh</label>
                        <div class="col-md-4 inputGroupContainer">
                            <div class="input-group" style="width:200px">
                                <span class="input-group-addon"><i class="fa fa-birthday-cake"></i></span>
                                <select name="day" class="form-control selectpicker">
                                    <option value="">[Ngày sinh]</option>
                                    <?php
                                    $begin=01;
                                    $end=31;
                  for($i=$begin;$i<=$end;$i++) {?>
                           <option><?php echo $i?></option>
                          
                  <?php }?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- Text input option DD/MM/YYYY-->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Tháng sinh</label>
                        <div class="col-md-4 inputGroupContainer">
                            <div class="input-group" style="width:200px">
                                <span class="input-group-addon"><i class="fa fa-birthday-cake"></i></span>
                                <select name="month" class="form-control selectpicker">
                                    <option value="">[Tháng sinh]</option>
                                    <?php
                                    $begin=01;
                                    $end=12;
                  for($i=$begin;$i<=$end;$i++) {?>
                           <option><?php echo $i?></option>
                          
                  <?php }?>
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Năm sinh</label>
                        <div class="col-md-4 inputGroupContainer">
                            <div class="input-group" style="width:200px">
                                <span class="input-group-addon"><i class="fa fa-birthday-cake"></i></span>
                                <select name="year" class="form-control selectpicker">
                                    <option value="">[Năm sinh]</option>
                                <?php
                                    $begin=1900;
                                    $end=2020;
                  for($i=$begin;$i<=$end;$i++) {?>
                           <option><?php echo $i?></option>
                          
                  <?php }?>
                    
                                </select>
                            </div>
                        </div>
                    </div>
                   
                    
                    <!-- Text input -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Bạn sống tại</label>
                        <div class="col-md-4 selectContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                <select name="country" class="form-control selectpicker">
                                    <option value="<?php if (isset($_POST['country'])) echo $_POST['country'] ?>">--Chọn thành phố--</option>
                                    <option>TPHCM</option>
                                    <option>Hà Nội</option>
                                    <option>Cần Thơ</option>
                                    <option>Bình Dương</option>
                                    <option>Bình Phước</option>
                                    <option>Đồng Nai</option>
                                    <option>Đà Lạt</option>
                                    <option>Đà Nẵng</option>
                                    <option>Quảng Ninh</option>
                                </select>
                            </div>
                        </div>
                    </div>
        

                    <div class="col-md-12 infomation-title">
                        <h5>Thông tin tài khoản</h5>
                    </div>
                    <!-- Text input-->
        
                    <div class="form-group">
                        <label class="col-md-4 control-label">Tên đăng nhập</label>
                        <div class="col-md-4 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user-circle"></i></span>
                                <input name="user_name"  value="<?php if (isset($_POST['user_name'])) echo $_POST['user_name'] ?>"
                                placeholder="Nhập tên đăng nhập" class="form-control" type="text">
                            </div>
                        </div>
                    </div>
        
                    <!-- Text input-->
        
                    <div class="form-group">
                        <label class="col-md-4 control-label">Mật khẩu</label>
                        <div class="col-md-4 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock" style="width: 15px;"></i></span>
                                <input name="user_password" placeholder="Nhập mật khẩu" value="<?php if (isset($_POST['user_password'])) echo $_POST['user_password'] ?>" 
                                class="form-control" type="password">
                            </div>
                        </div>
                    </div>
        
                    <!-- Text input-->
        
                    <div class="form-group">
                        <label class="col-md-4 control-label">Xác nhận mật khẩu</label>
                        <div class="col-md-4 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock" style="width: 15px;"></i></span>
                                <input name="confirm_password" value="<?php if (isset($_POST['confirm_password'])) echo $_POST['confirm_password'] ?>" 
                                 placeholder="Nhập xác nhận mật khẩu" class="form-control"
                                    type="password">
                            </div>
                        </div>
                    </div>
        
                    <!-- Text input-->
                <div class="col-md-12 infomation-title">
                    <h5>Mã kiểm tra</h5>
                   <img src="https://www.tsohost.com/assets/uploads/blog/capcha.jpeg">
                    
                </div>
              
               
             <div class="form-group">
                <label class="col-md-4 control-label">Nhập mã kiểm tra</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <input name="reCaptcha" placeholder="Nhập mã kiểm tra"
                        value="<?php if (isset($_POST['reCaptcha'])) echo $_POST['reCaptcha'] ?>"
                         class="form-control" type="password">
                    </div>
                </div>
            </div>
                    
                    <!-- Select Basic -->
        
                    <!-- Success message -->
                    <div class="alert alert-success" role="alert" id="success_message">Success <i
                            class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>
        
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-6"><br>
                        <button type="submit"
                                class="btn btn-warning"  name="dangky" value="dangky" id="btn-res">&nbsp&nbsp&nbsp&nbspĐăng ký <span
                                    class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp</button>
                                    <button class="btn btn-danger"  id="return-res" onclick="window.location.href='register.php'">Huỷ đăng ký</button>
                                    <button class="btn btn-primary" id="return-log"   onclick="window.location.href='login.php'">Đăng nhập</button>
                        </div>
                    </div>
        
                </fieldset>
            </form>
        </div>
        </div><!-- /.container -->

   </div>