
    <div id="demo" class="carousel slide" data-ride="carousel">
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
        </ul>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="https://cdn.tgdd.vn/2019/11/banner/800-300-800x300-(9).png" alt="Los Angeles" >
                <div class="carousel-caption">
                    <h3>khuyến mãi tháng 11</h3>
                    <p>Tặng bạn iphone 11 pro Max khi mua nokia 1080</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="https://cdn.tgdd.vn/2019/11/banner/800-300-800x300-(15).png" alt="Chicago" width="1100" height="200">
                <div class="carousel-caption">
                    <h3>Sangsum A50S</h3>
                    <p>Cùng Đông Nhi nhen!</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="https://cdn.tgdd.vn/2019/11/banner/800-300-800x300-(10).png" alt="New York" width="1100" height="200">
                <div class="carousel-caption">
                    <h3>Điện thoại Redmi note 8 hot nhất tháng 11</h3>
                    <p>Trả góp 0%, tha hồ mua sắm</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
        </a>
    </div >
    <!-- 10 Sản phẩm mới-->
<div class="newProduct">
    <div class="col-sm-12 col-sm-6 col-sx-6">
        <div class="beta-products-list">
            <h4>Sản phẩm mới</h4>
            <hr>
            <div class="beta-products-Chi tiết sản phẩm">
                <p class="pull-left">có hơn 120 sản phẩm</p>
                <div class="clearfix"></div>
            </div>
            <!--FETCH SẢN PHẨM MỚI NHẤT-->
            <div class="row">
                <?php foreach ($data['newProducts'] as $product) : ?>
                <div class="single-item">
                    <div class="single-item-header">
                        <a href="sanpham/<?= $product->MaSanPham ?>"><img
                            src="public/product/<?= $product->HinhURL?>"
                            alt=""></a>
                    </div>
                    <div class="single-item-body">
                        <p class="single-item-title"><?= $product->TenSanPham ?></p>
                        <p class="single-item-price">
                            <strong><?= number_format($product->GiaSanPham) ?> VNĐ</strong>
                        </p>
                    </div>
                    <div class="single-item-caption">
                        <a class="beta-btn info" href="sanpham/<?= $product->MaSanPham ?>">Chi tiết sản phẩm <i class="fa fa-chevron-right"></i></a>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<!-- 10 Sản phẩm bán chạy nhất-->
<div class="best-seller-products">
   <!--FETCH SẢN PHẨM BÁN CHẠY NHẤT-->
   <div class="col-md-12 col-sm-6 col-sx-6">
      <div class="beta-products-list">
         <h4>Sản phẩm bán chạy nhất</h4>
         <hr>
         <div class="beta-products-Chi tiết sản phẩm">
            <p class="pull-left">có hơn 100 sản phẩm</p>
            <div class="clearfix"></div>
         </div>
         <div class="row">
            <?php foreach ($data['bestProducts'] as $product) : ?>
               <div class="single-item">
                  <div class="single-item-header">
                     <a href="sanpham/<?= $product->MaSanPham ?>"><img
                        src="public/product/<?= $product->HinhURL?>"
                        alt=""></a>
                  </div>
                  <div class="single-item-body">
                     <p class="single-item-title"><?= $product->TenSanPham ?></p>
                     <p class="single-item-price">
                        <strong><?= number_format($product->GiaSanPham) ?> VNĐ</strong>
                     </p>
                  </div>
                  <div class="single-item-caption">
                     <a class="beta-btn info" href="sanpham/sanpham/<?= $product->MaSanPham ?>">Chi tiết sản phẩm <i class="fa fa-chevron-right"></i></a>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <?php endforeach ?>
         
         </div>
      </div>
   </div>
</div>
