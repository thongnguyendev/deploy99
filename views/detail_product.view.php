    
    <link rel="stylesheet" type="text/css" href="public/css/style-detailsProduct.css">
    <script src="public/js/style-detailsProduct.js"></script> 
    <!--Content-->
<div id="content">
    <div class="main-container col1-layout">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <div class="details-item">               
                        <div class="details-item-header">
                           <img src="public/product/<?= $data['p']->HinhURL ?>"
                                   data-id="<?=$product->MaSanPham?>" alt="">
                        </div>
                        <div class="details-item-body">
                            <br>
                            <p class="single-item-title"><?=$data['p']->TenSanPham ?></p>
                            <p class="single-item-price">                               
                               <strong><?= number_format($data['p']->GiaSanPham) ?> VNĐ</strong>                              
                            </p>
                        </div>
                        </div>
                        </div>                 
                          <div class="col-md-7 col-sx-7">
                            <div class="short-description">
                                <h4>Chi tiết sản phẩm</h4>
                                <div class="info-product-details">
                                    <div><strong>-Mô tả :</strong> <?= $data['p']->MoTa ?></div>
                                    <div><strong>-Số lượng bán:</strong> <?= $data['p']->SoLuongBan ?> sản phẩm</div>
                                    <div><strong>-Số lược xem:</strong> <?= $data['p']->SoLuongXem ?> lược</div>
                                    <div><strong>-Xuất xứ: </strong><?= $data['p']->XuatXu ?> </div>
                                    <div><strong>-Nhà sản xuất:</strong> <?= $data['p']->NhaSanXuat ?> </div>
                                </div>
                            </div>
                            <hr class="hr">
                            <div class="col-8">
                            <div class="product-variation">
                                <form action="shopping-cart.php" method="post">    
                                    
                                        <label for="qty">Nhập số lượng: </label>
                                        <div class="form-group-row">
                                            <input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty"
                                                name="qty">
                                                <button class="btn btn-outline-warning" type="button"
                                                    onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) && qty>1) result.value--;return false;"><i
                                                        class="fa fa-minus">&nbsp;</i></button>
                                                <button class="btn btn-outline-secondary" type="button"
                                                    onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;"><i
                                                        class="fa fa-plus">&nbsp;</i></button>
                                    <button class="btn btn-outline-success" title="Add to Cart" type="submit"
                                        data-id="<?= $data['p']->MaSanPham ?>" >
                                       
                                            <i class="fa fa-shopping-cart"></i> Thêm vào giở hàng
                                    </button>
                                </div>
                               
                                </form>
                            </div>
                            </div>
                       </div>
                        </div>
                </div>
             </div>
            </div>
        </div>
    </div>
            
        <!--5 Sản phẩm cùng loại-->
        <div class="product_type">
            <div class="container">
                <div class="col-sm-12">
                    <div class="beta-products-list">
                        <hr class="hr-product-type">
                        <h4>Sản phẩm cùng loại</h4>
                        <hr>
                     
                        <!--5 sản phẩm -->
                        <div class="row">
                                <?php foreach ($data['relatedProducts'] as $product) : ?>
                                <div class="item">
                                    <div class="item-header">
                                        <a href="sanpham/<?= $product->MaSanPham ?>"><img
                                                src="public/product/<?= $product->HinhURL?>"
                                                alt=""></a>
                                    </div>
                                    <div class="single-item-body">
                                        <p class="item-title"><?=$product->TenSanPham?></p>
                                        <p class="item-price">
                                            <strong><?=number_format($product->GiaSanPham)?>VNĐ</strong>
                                        </p>
                                    </div>
                                    <div class="item-caption">
                                    
                                        <a class="beta-btn primary" href="sanpham/<?= $product->MaSanPham ?>">Chi tiết sản phẩm </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
       </div>
 </div>