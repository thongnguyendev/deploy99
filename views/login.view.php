
<!--thêm css và js -->
    <link rel="stylesheet" type="text/css" href="public/css/style-login.css">
    
<!--Phần code php check đk-->
<?php
//session_start();
// gọi lại file DBconnect
require_once'config.php';
//<script src="public/js/style-login.js"></script>

if(isset($_POST['return'])){{
    header('Location: index.php');
    
}}

// kiểm tra đăng nhập
if (isset($_POST['login'])) {
	$errMsg = ''; // khai báo biên báo lỗi
	// Get data from FORM
	$username = $_POST['uname'];
	$password = $_POST['pswd'];
	// nếu username rỗng thì sẽ báo lỗi là phải nhập username vào, nếu mật khẩu cũng như trên, phải nhập mật khẩu nữa thì ms thực thi
	if ($username == '')
		$errMsg = 'Nhập Username vào!';
	if ($password == '')
		$errMsg = 'Nhập Password vào!';
	if ($password == '' && $username == '')
		$errMsg = 'Nhập username và password ^-^';
	if ($errMsg == '') {
		try {
	//	 $password=	md5($password);
		$strQuery = 'SELECT * FROM TaiKhoan WHERE BiXoa = 0 AND 
        TenDangNhap = :TenDangNhap  ';
			// lấy dữ liệu ở database lên
			$stmt = $connect->prepare($strQuery);
			
		$check=	$stmt->execute(Array(
				':TenDangNhap' => $username,
			));
			//print_r($check);
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			//print_r  ($data);
			

			// nếu mà USER  lấy  ở csdl k giống thì nó báo lỗi
			if ($username != $data['TenDangNhap']) {
				$errMsg = "User $username tìm không thấy."; // K THỂ TÌM THẤY USER NÀY // DO SAI HOẶC CHỮ IN HOA
			} else {

				// nếu mà dữ liệu đúng ở database đúng hết thì 
				if ($password == $data['MatKhau']) {
					$_SESSION['uname'] = $data['TenDangNhap'];
                    $_SESSION['pswd'] = $data['MatKhau'];
                  // nếu mà user=admin và mk =admin thì chuyển sang trang admin
                  if($username =='Admin'&&$password=='Admin'){
                      echo "<script>window.location.replace('public/admin/index.html');</script>";
                  }
                  else{
                     // nó sẽ load tới local: user.php
				     echo "<script>window.location.replace('user.php');</script>";
                  }
					
					//echo 'Login success !! Welcome to ' . $data['TenHienThi']; // này là nó  load lại trang chính nó
				     exit; // thoát
                } 
                else{
                    $errMsg = 'Mật khẩu không chính xác.'; // còn k thì mật khẩu k chính xác
                }
			}
		} catch (PDOException $e) {
			$errMsg = $e->getMessage();
		}
	}
}

?>

 <!--phần code nội dung đăng nhập-->   
<div id="content">
    <div class="container">
    <?php // thông báo lỗi trên đây
				if (isset($errMsg)) {
					echo '<div style="color:#FF0000;text-align:center;font-size:17px;">' . $errMsg . '</div>';
				}
		?>
         <div class="row justify-content-center">
            <div class="col-md-4 col-sm-8 col-sx-12">
        <h3 class="info-text">Thông tin đăng nhập</h3>
             
        
        <form action="" method="post"  class="needs-validation" novalidate>
            <div class="form-group">
                <label for="uname">Tên đăng nhập</label>
                <input type="text" class="form-control" id="uname" placeholder="Nhập vào Username "
                 name="uname" required  value="<?php if (isset($_POST['uname'])) echo $_POST['uname'] ?>"
                 >
                <div class="valid-feedback">Hợp lệ</div>
                <div class="invalid-feedback">Nội dung không được bỏ trống..</div>
            </div>
            <div class="form-group">
                <label for="pwd">Mật khẩu</label>
                <input type="password" class="form-control" id="pwd" placeholder="Nhập vào mật khẩu"
                 name="pswd" value="<?php if (isset($_POST['pswd'])) echo $_POST['pswd'] ?>" required>
                <div class="valid-feedback">Hợp lệ</div>
                <div class="invalid-feedback">Nội dung không được bỏ trống.</div>
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="remember" required> Tôi đồng ý nhớ mật khẩu
                    <div class="valid-feedback">Hợp lệ</div>
                    <div class="invalid-feedback">Phải check vào mới tiếp tục</div>
                </label>
            </div>
            <button type="submit" class="btn btn-primary"  name="login" id="login">Đăng nhập</button>
            <button type="button" class="btn btn-danger" id="return"  name="return"
           ">Trở về</button>
        </form>
    </div>
</div>
    </div>
</div>