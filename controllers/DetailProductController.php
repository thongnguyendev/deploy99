<?php

require_once'Controller.php';
require_once'models/DetailProductModel.php';

class DetailProductController extends Controller{
  
    function DatailProduct()
    {
       if(!isset($_GET['id'])){
            header('Location:error.php');
            return;
        }
        $url = $_GET['id'];
        $model = new DetailProductModel();
        $product = $model->getProductDetail($url);
        //print_r($product);
        $title = $product->TenSanPham;
        $idType = $product->MaLoaiSanPham;
        $relatedProducts = $model->getRelatedProducts($url,$idType);
       // print_r($relatedProducts);
        $data=[
             'p'=>$product,
             'relatedProducts'=>$relatedProducts

        ];
        

        return parent::loadView('detail_product',$title, $data);
}
    
}
?>