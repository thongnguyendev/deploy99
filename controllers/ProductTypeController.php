<?php

require'Controller.php';
require_once'models/ProductTypeModel.php';
class ProductTypeController extends Controller{
    function ProductType (){
       $model=new ProductTypeModel();
       $name_type=$model->nameType();
       $product_type=$model->LoadProductByType();
       //print_r ($name_type);
        //print_r($product_type);
       if(!isset($_GET['id'])){
            header('Location: error.php');
            return;
        }
        $url = $_GET['id'];
        $type = $model->getProductTypeByUrl( $url);
      //  print_r($model->getProductTypeByUrl( $url));
        $products = $model->getProductByType($url);
        //print_r($products);
        $data = [
            'nameType'=> $name_type,
            'product_type'=>$product_type,
            'product'=>$products,
            'type'=>$type
        ];
        
        //print_r($products);
        return parent::loadView('product_type','Sản phẩm theo loại',$data);
    }
    
}
?>