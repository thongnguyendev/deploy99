<?php

require'Controller.php';
require_once 'models/ProductManuModel.php';
class ProductManufactureController extends Controller{
    function ProductManufacture (){
       $model=new ProductManuModel();
       $name_manu=$model->nameType();
       $product_manu=$model->LoadProductByManu();
         if(!isset($_GET['url'])){
            header('Location: error.php');
            return;
        }
        $url = $_GET['url'];
        $manu = $model->getProductManuByUrl( $url);
        $products = $model->getProductByManu($url);
        //print_r( $model->getProductByManu($url));
       // print_r( $model->getProductManuByUrl($url));
        $data = [
            'name_manu'=> $name_manu,
            'product_manu'=>$product_manu,
            'manu'=>$manu,
            'product'=>$products
        ];

        return parent::loadView('product_manufacture','Sản phẩm theo hãng',$data);
    }
}
?>