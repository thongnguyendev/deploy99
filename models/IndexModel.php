<?php
require_once'DBConnect.php';
class IndexModel extends DBConnect{
    
    // những sản phẩm bán chạy nhất
    function selectBestProducts()
    {        
        $sql = "SELECT * FROM SanPham WHERE BiXoa = 0 ORDER BY SoLuongBan DESC LIMIT 0, 10";
        return $this->getMoreRow($sql);
    }
    //những sản phẩm mới nhất
    
    function newProducts()
    {
        $sql= "SELECT * FROM SanPham WHERE BiXoa = 0 ORDER BY NgayNhap DESC LIMIT 0, 10";
        return $this->getMoreRow($sql);
    }
    
    
}
?>